import openmc.data
import numpy as np
import glob
from pathlib import Path
from multiprocessing import Pool

library = openmc.data.DataLibrary()
nuc = "O16"
file = sorted(Path('tmc').glob('O016-n.ace_*'))


for i in range(len(file)):
	data = openmc.data.IncidentNeutron.from_ace(file[i], 'mcnp')
	data.name = f"{nuc}-{i}"
	h5_file = f"{nuc}-{i}.h5"
	print(f'Writing {h5_file}...')
	data.export_to_hdf5(h5_file, 'w', libver='latest')
	library.register_file(h5_file)

# Write cross_sections.xml
library.export_to_xml(f'cross_sections_TMC-{nuc}.xml')
