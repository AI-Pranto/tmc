import openmc.data
import numpy as np
import glob
from pathlib import Path
from multiprocessing import Pool

library = openmc.data.DataLibrary()
nuc = "U238"
file = sorted(Path('sandy_rand/endf/U238').glob('*'))

library = openmc.data.DataLibrary()

def process_neutron(path, i):
    data = openmc.data.IncidentNeutron.from_njoy(path, stdout=True)
    data.name = f"{nuc}-{i}"
    h5_file = f'{data.name}-{i}.h5'
    print(f'Writing {h5_file} ...')
    data.export_to_hdf5(h5_file, 'w', libver= 'latest')

with Pool() as pool:
    results = []
    for i in range(len(file)):
        func_args = (file[i], i)
        r = pool.apply_async(process_neutron, func_args)
        results.append(r)

    for r in results:
        r.wait()


# Register with library
for p in sorted(glob.glob('tmc/*.h5')):
    library.register_file(p)

# Write cross_sections.xml
library.export_to_xml(f'cross_sections_TMC-{nuc}.xml')
